//
//  EventBookCoordinator.swift
//  Tribute
//
//  Created by saurabh-pc on 10/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
protocol EventBookCoordinatorDelegate:class {
    func dissmissEventBook()
    func dissmissEventBookgotoDashbosard()

}
class EventBookCoordinator: Coordinator {
    weak var delegate:EventBookCoordinatorDelegate!
    weak var navigationController:UINavigationController!
    var event:Events!
    init(_navigationController:UINavigationController,_event:Events) {
        navigationController = _navigationController
        event = _event
    }
    func start() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let eventsBookController = storyboard.instantiateViewController(withIdentifier: "EventBookViewController") as! EventBookViewController
        eventsBookController.viewModel = EventBookViewModel(_evnt: event)
        eventsBookController.viewModel.delegateCoordinate = self
       // eventsViewController.viewModel = EventViewModel.init(_events: events)
       // eventsViewController.viewModel.delegateCoordinator = self
        navigationController.pushViewController(eventsBookController, animated: true)
    }
}
extension EventBookCoordinator:EventBookViewModelCoordinateDelegate
{
    func dissmisseventBookandgotolist() {
        navigationController.popToRootViewController(animated: true)
        delegate?.dissmissEventBookgotoDashbosard()
    }
    
    func dissmisseventBook() {
        delegate?.dissmissEventBook()
        navigationController.popViewController(animated: true)
    }
    
    
   
    
    
}
