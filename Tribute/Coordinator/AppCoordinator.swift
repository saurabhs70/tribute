//
//  AppCoordinator.swift
//  Tribute
//
//  Created by saurabh-pc on 09/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
import LGSideMenuController

class AppCoordinator: Coordinator {
    var window:UIWindow!
    var mainnavigationController:LGSideMenuController!
    var navigatincontroller:UINavigationController!
    var coordinators = [String:Coordinator]()
    
    let INITIAL_KEY = "initial"
    let DASHBOARD_KEY = "dashboard"
    init(_window:UIWindow) {
      window = _window
    }
    func start() {
        coordinators.removeAll()
        _ = DataFeedar.init()

        if checkUser() {
            showDashboard()
            return
        }
        showInitial()
    }
}
extension  AppCoordinator:InitialCoordinatorDelegate
{
    func dismiss() {
        coordinators[INITIAL_KEY] = nil
        showDashboard()
    }
    func showInitial()  {
        navigatincontroller = UINavigationController.init()
        mainnavigationController = LGSideMenuController.init()
        let initialCoordinator = InitialCoordinator.init(_navigationController: navigatincontroller, _window: window, _mainnavigationController: mainnavigationController)
        initialCoordinator.delegate = self
        initialCoordinator.start()
        coordinators[INITIAL_KEY] = initialCoordinator
    }
    
}
extension AppCoordinator:DashboardCoordinatorDelegate
{
    func dismiss1() {
        coordinators[INITIAL_KEY] = nil
        showDashboard()
    }
    func showDashboard()  {
        navigatincontroller = UINavigationController.init()
        mainnavigationController = LGSideMenuController.init()
        let dashboardcoordinator = DashboardCoordinator.init(_navigationController: navigatincontroller, _window: window, _mainnavigationController: mainnavigationController)
        dashboardcoordinator.delegate = self
        dashboardcoordinator.start()
        coordinators[DASHBOARD_KEY] = dashboardcoordinator

    }
    
}
extension AppCoordinator
{
   fileprivate func checkUser() -> Bool {
        let user =    UserDefaults.standard.object(forKey: "loggedinPhone") as? String
        if let _ =  user {
             return true
        }
        return false
    }
}
