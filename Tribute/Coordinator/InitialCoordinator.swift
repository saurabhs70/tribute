//
//  InitialCoordinator.swift
//  Tribute
//
//  Created by saurabh-pc on 09/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
import LGSideMenuController
protocol InitialCoordinatorDelegate:class {
    func dismiss()
    
}
class InitialCoordinator: Coordinator {
    weak var window:UIWindow!
    weak var navigationController:UINavigationController!
    weak  var mainnavigationController:LGSideMenuController!
    weak var delegate:InitialCoordinatorDelegate!
    var logincoordinator:LoginCoordinator!
    init(_navigationController:UINavigationController,_window:UIWindow,_mainnavigationController:LGSideMenuController) {
        navigationController = _navigationController
        window =  _window
        mainnavigationController = _mainnavigationController
        
    }
    func start() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let initiacoordinastor = storyboard.instantiateViewController(withIdentifier: "InitialViewController") as! InitialViewController
        initiacoordinastor.viewModel = InitialViewModel()
        initiacoordinastor.viewModel.delgateCoordinate = self
        navigationController?.setViewControllers([initiacoordinastor], animated: true)
        let sidemenu = storyboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        mainnavigationController = LGSideMenuController.init(rootViewController: navigationController, leftViewController: sidemenu, rightViewController: nil)
        mainnavigationController?.leftViewWidth = 250
        mainnavigationController.leftViewPresentationStyle = .scaleFromBig
       // navigationController.navigationBar.barTintColor = UIColor.init(red: (255/255), green: (45/255), blue: (127/255), alpha: 1)
        
        navigationController.navigationBar.isHidden = true
        // mainnavigationController.leftViewBackgroundImage = UIImage.init(named: "imageLeft")
        mainnavigationController.leftViewBackgroundColor = UIColor.init(red: (255/255), green: (45/255), blue: (99/255), alpha: 1)
        window?.rootViewController = mainnavigationController

    }
    

}
extension InitialCoordinator:InitialViewModelCoordinaleDelegate
{
    func pushtoLoginPage(isUser: Bool) {
        logincoordinator = LoginCoordinator.init(_navigationController: navigationController, _isuser: isUser)
        logincoordinator.delegate = self
        logincoordinator.start()
    }
    
    
}
extension InitialCoordinator:LoginCoordinatorDelegate
{
    func dismiss() {
        logincoordinator = nil
        delegate?.dismiss()
    }
    
    
}
