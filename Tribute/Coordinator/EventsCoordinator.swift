//
//  EventsCoordinator.swift
//  Tribute
//
//  Created by saurabh-pc on 10/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
protocol EventsCoordinatorDelegate:class {
    func dismissEventist()
}
class EventsCoordinator:Coordinator {
    weak var navigationcontroller:UINavigationController!
    weak var delegate:EventsCoordinatorDelegate?
    var events:NSSet
    var eventbookCoordinate:EventBookCoordinator!
    init(_navigationcontroller:UINavigationController,_events:NSSet) {
        navigationcontroller = _navigationcontroller
        events = _events
    }
    func start() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let eventsViewController = storyboard.instantiateViewController(withIdentifier: "EventsViewController") as! EventsViewController
         eventsViewController.viewModel = EventViewModel.init(_events: events)
        eventsViewController.viewModel.delegateCoordinator = self
        navigationcontroller.pushViewController(eventsViewController, animated: true)
        
    }
    

}
extension EventsCoordinator:EventViewModelCoordinatorDelegate
{
    
    
    func gotoBookCoordinate(event: Events) {
        eventbookCoordinate = EventBookCoordinator.init(_navigationController: navigationcontroller, _event: event)
        eventbookCoordinate.delegate = self
        eventbookCoordinate.start()
    }
    
    func dismissEventVc() {
        navigationcontroller.popViewController(animated: true)
        delegate?.dismissEventist()
    }
    
    
    
    
}
extension EventsCoordinator:EventBookCoordinatorDelegate
{
    func dissmissEventBookgotoDashbosard() {
       // eventbookCoordinate?.dissmisseventBookandgotolist()
        eventbookCoordinate = nil
        delegate?.dismissEventist()
       // navigationcontroller?.popViewController(animated: true)

    }
    
    func dissmissEventBook() {
        eventbookCoordinate = nil
        
    }
    
    
}
