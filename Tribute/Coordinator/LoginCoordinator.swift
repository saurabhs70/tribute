//
//  LoginCoordinator.swift
//  Tribute
//
//  Created by saurabh-pc on 09/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
protocol LoginCoordinatorDelegate:class {
    func dismiss()
}
class LoginCoordinator: Coordinator {
    weak var navigationController:UINavigationController!
    weak var delegate:LoginCoordinatorDelegate?
    var  loginVc:LoginViewController!
    var isuser:Bool!
    init(_navigationController:UINavigationController,_isuser:Bool) {
        navigationController = _navigationController
        isuser = _isuser
    }
    func start() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
       loginVc  = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        loginVc.viewModel = LoginViewModel()
         loginVc.viewModel.isUser = isuser
        loginVc.viewModel.delegateCoordinate = self
        navigationController.navigationBar.isHidden = true
        self.navigationController.present(loginVc, animated: true, completion: nil)
    }
    

}
extension LoginCoordinator:LoginViewModelCoordinateDelegate
{
    func present() {
        loginVc = nil
        delegate?.dismiss()
    }
    func dissmissLoginvc() {
        self.navigationController.dismiss(animated: true, completion: nil)
        //delegate?.dismiss()
    }
    
}
