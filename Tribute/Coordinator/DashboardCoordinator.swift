//
//  DashboardCoordinator.swift
//  Tribute
//
//  Created by saurabh-pc on 09/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
import LGSideMenuController
protocol DashboardCoordinatorDelegate:class {
    func dismiss1()
}

class DashboardCoordinator: Coordinator {
    
    weak var window:UIWindow!
    weak var navigationController:UINavigationController!
    weak  var mainnavigationController:LGSideMenuController!
    weak var delegate:InitialCoordinatorDelegate!
    var  rateuscoordinator:RateUsCoordinator!
    var  eventlistCoordinator:EventsCoordinator!
    init(_navigationController:UINavigationController,_window:UIWindow,_mainnavigationController:LGSideMenuController) {
        navigationController = _navigationController
        window =  _window
        mainnavigationController = _mainnavigationController
        
    }
    func start() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let dashboard = storyboard.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
        dashboard.viewModel = DashboardViewModel()
        dashboard.viewModel.delegateCoordinate = self
        navigationController?.setViewControllers([dashboard], animated: true)
        let sidemenu = storyboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        mainnavigationController = LGSideMenuController.init(rootViewController: navigationController, leftViewController: sidemenu, rightViewController: nil)
        mainnavigationController?.leftViewWidth = 250
        mainnavigationController.leftViewPresentationStyle = .scaleFromBig
        // navigationController.navigationBar.barTintColor = UIColor.init(red: (255/255), green: (45/255), blue: (127/255), alpha: 1)
        
        navigationController.navigationBar.isHidden = true
        // mainnavigationController.leftViewBackgroundImage = UIImage.init(named: "imageLeft")
        mainnavigationController.leftViewBackgroundColor = UIColor.init(red: (255/255), green: (45/255), blue: (99/255), alpha: 1)
        window?.rootViewController = mainnavigationController
        
    }
}
extension DashboardCoordinator:DashboardViewModelCoordinateDelegate
{
    func gotoeventook() {
//        va
//        let  eventbookCoordinate = EventBookCoordinator.init(_navigationController: navigationController, _event: <#Events#>)
//        //eventbookCoordinate.delegate = self
//        eventbookCoordinate.start()
    }
    
    
    func gotoEvent(event: NSSet) {
        eventlistCoordinator = EventsCoordinator.init(_navigationcontroller: navigationController, _events:event )
        eventlistCoordinator.delegate = self
        eventlistCoordinator.start()
    }
    
    func gotoRating(artist: Artist) {
        rateuscoordinator  = RateUsCoordinator.init(_navigationController: navigationController, _artist: artist)
        rateuscoordinator.delegate = self
        rateuscoordinator.start()
    }
    
    
    
}
extension DashboardCoordinator:RateUsCoordinatorDelegate
{
    func dismissRateus() {
        rateuscoordinator = nil
    }
    
    
}
extension DashboardCoordinator:EventsCoordinatorDelegate
{
    func dismissEventist() {
        eventlistCoordinator = nil
    }
    
    
}
