//
//  RateUsCoordinator.swift
//  Tribute
//
//  Created by saurabh-pc on 10/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
protocol RateUsCoordinatorDelegate:class {
    func dismissRateus()
}
class RateUsCoordinator:Coordinator {

    weak var navigationController:UINavigationController!
    weak var delegate:RateUsCoordinatorDelegate?
    weak var artist:Artist!
    init(_navigationController:UINavigationController,_artist:Artist) {
        navigationController = _navigationController
        artist = _artist
    }
    func start() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let rateus = storyboard.instantiateViewController(withIdentifier: "RateUsViewController") as! RateUsViewController
        rateus.viewModel = RateUsViewModel(_artist: artist)
         rateus.viewModel.delegateCoordinate = self

        navigationController.present(rateus, animated: true, completion: nil)
    }
}
extension RateUsCoordinator:RateUsViewModelCoordinateDelegate
{
    func dismissRate() {
        navigationController?.dismiss(animated: true, completion: nil)
        delegate?.dismissRateus()
    }
    

}
