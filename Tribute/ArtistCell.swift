//
//  ArtistCell.swift
//  Tribute
//
//  Created by saurabh-pc on 09/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
import Cosmos
protocol didupdateCell:class {
    func updateinfo(artist:Int)
}
class ArtistCell: UITableViewCell {
    weak var delegate:didupdateCell?
    @IBOutlet weak var userocation: UILabel!
    @IBOutlet weak var txtname: UILabel!
    @IBOutlet weak var userimg: UIImageView!
    @IBOutlet weak var btnOpenRate: UIButton!
    
    @IBOutlet weak var viewRate: CosmosView!
    @IBOutlet weak var lableReview: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userimg.layer.cornerRadius =  userimg.frame.size.width/2
        userimg.clipsToBounds = true
        viewRate.settings.updateOnTouch = false
            }
    @objc func buttonAction(sender:UIButton) {
        delegate?.updateinfo(artist: sender.tag)
    }
    func setupCell(artist:Artist,index:Int)  {
        userimg.image = UIImage.init(named: artist.image!)
       userocation.text = artist.location
        txtname.text = artist.name
        lableReview.text = "\(artist.comments?.count ?? 0) reviews"
        viewRate.rating = findmaxrate(artist: artist)
        btnOpenRate.tag = index
            btnOpenRate.addTarget(self, action: #selector((buttonAction)), for: UIControlEvents.touchUpInside)
 
    }
    func findmaxrate(artist:Artist) -> Double {
        //let userto = ((userinfo as! Comment).value(forKey: "creater") ?? "") as! User
        var max:Double = 0
        for rate in artist.ratings! {
            let userto = (rate as! Rating)//.value(forKey: "rating") as! Double
            if(userto.rating >= max )
            {
               max = userto.rating
            }
        }
        return max
        
       // let ratings = artist.ratings
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
