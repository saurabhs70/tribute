//
//  EventBookViewController.swift
//  Tribute
//
//  Created by saurabh-pc on 10/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit

class EventBookViewController: UIViewController {
    var viewModel:EventBookViewModel!
    @IBOutlet weak var imguser: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var eventview: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = viewModel.name
        lblDescription.text = viewModel.descriptionevent
        imguser.layer.cornerRadius = imguser.frame.size.width/2
        imguser.clipsToBounds = true
        imguser.image = UIImage.init(named: "cardi-b")
        //eventview.dropShadow()
//eventview.dropShadow(color: .black, opacity: 1, offSet: CGSize(width: -1, height: 1))
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBookTapped(_ sender: Any) {
        viewModel.dissmisseventBookandgotolist()

    }
    @IBAction func btnBackTapped(_ sender: Any) {
        viewModel.dissmisseventBook()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
