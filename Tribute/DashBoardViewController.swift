//
//  DashBoardViewController.swift
//  Tribute
//
//  Created by saurabh-pc on 09/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit

class DashBoardViewController: UIViewController {
    @IBOutlet weak var tbllist: UITableView!
    var viewModel:DashboardViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
viewModel.delegateView = self
    
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        tbllist.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnGotoMenuTapped(_ sender: Any) {
        self.showLeftViewAnimated(true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DashBoardViewController:DashboardViewModelViewDelegate
{
    func didErrorAlreadyRated(errormessage: String) {
        let alert = UIAlertController(title: "Error!", message: errormessage, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        self.present(alert, animated: true, completion: nil)

    }
    
    
}
extension DashBoardViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArtistCell", for: indexPath) as! ArtistCell
        let artist = viewModel.prepareCell(index: indexPath.row)
        cell.delegate = self
        cell.setupCell(artist: artist, index: indexPath.row)
            cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberofRows
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.gotoEventList(index: indexPath.row)
    }
}
extension DashBoardViewController:didupdateCell
{
    func updateinfo(artist: Int)  {
        viewModel.gotoRating(index: artist)

    }
    
    
}
