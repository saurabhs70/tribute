//
//  User+CoreDataProperties.swift
//  Tribute
//
//  Created by saurabh-pc on 10/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var id: Int16
    @NSManaged public var isUser: Bool
    @NSManaged public var number: String?

}
