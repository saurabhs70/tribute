//
//  DataBaseManager.swift
//  Tribute
//
//  Created by saurabh-pc on 09/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
import CoreData
class DataBaseManager: NSObject {
    static let sharedInstance = DataBaseManager()

    func loadArtist(lists:Array<Any>)  {
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        for  dict in lists {
            let context=delegate.persistentContainer.viewContext
            let nam:NSDictionary=dict as! NSDictionary
            let subid:Int64 = Int64((nam.object(forKey: "id")! as! NSString).intValue)
            let subname = nam.object(forKey: "name")!
            let image = nam.object(forKey: "image")
            let location = nam.object(forKey: "location")
            let events:[Any] = nam.object(forKey: "events") as! [Any]
           // let evensets = NSSet.init(array: events as! [Any])

            let addsubject = NSEntityDescription.insertNewObject(forEntityName: "Artist", into: context)
            addsubject.setValue(subid, forKey: "id")
            addsubject.setValue(subname, forKey: "name")
            addsubject.setValue(image, forKey: "image")
            addsubject.setValue(location, forKey: "location")
            //addsubject.setValue(evensets, forKey: "events")
         let finaldata = prepateAssociateddata(eventsdata: events)
            let mySet = NSSet(array : finaldata)

            addsubject.setValue(mySet, forKey: "events")
            

            do
            {
                try context.save()
                print("")
            }
            catch
            {
                print("")
                
            }
        }
        
        let lists = getall(stringforentity: "Artist") as! [Artist]
        print("\(lists)")
        
    }
    func prepateAssociateddata(eventsdata:[Any])->[Events]  {
        var events = [Events]()
        for eventof in eventsdata {
            let eventfinal:[String:Any] = eventof as! [String:Any]
            if let totalfup = (eventfinal["id"] as? NSString)?.intValue, let _nameEvent = (eventfinal["name"] as? NSString),let descriptionEvent = (eventfinal["eventdescription"] as? NSString)  {
                let event = addEvent(idEvent: Int(totalfup), nameEvent: _nameEvent as String , descriptionEvent: descriptionEvent as String)
                events.append(event!)
            }
//            let _idEvent = eventfinal["id"] as! Int
//            let _nameEvent = eventfinal["name"] as! String //eventfinal.object(forKey: "name") as! String
//            let _descriptionEvent = eventfinal["eventdescription"] as! String // eventfinal.object(forKey: "eventdescription") as! String
//            let event = addEvent(idEvent: _idEvent, nameEvent: _nameEvent , descriptionEvent: _descriptionEvent)
//            events.append(event!)
        }
        return events
    }
    //MARK:- Get all entity
    func getall(stringforentity:String)->Array<Any>?  {
        let array_users:Array<Any>?;
        let appdel=UIApplication.shared.delegate as! AppDelegate;
        let context=appdel.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: stringforentity)
        fetchRequest.returnsObjectsAsFaults=false
        //fetchRequest.predicate=NSPredicate(format: "user= %@","")
        do {
            array_users = try context.fetch(fetchRequest)
            return array_users!;
            //print("\(array_users)")
        }
        catch
        {
            return nil;
        }
        //var fetchreq=NSFetchRequest(entityname:"Vehicle")
        
    }
    
    func addRating(rating:Double,user:User) -> Rating? {
        
            
            
            let appdel = UIApplication.shared.delegate as! AppDelegate
            let context = appdel.persistentContainer.viewContext
            let newclass = NSEntityDescription.insertNewObject(forEntityName: "Rating", into: context)
            newclass.setValue(rating, forKey: "rating")
            newclass.setValue(pkid(), forKey: "id")
           newclass.setValue(user, forKey: "creater")
            do
            {
                try context.save()
                return newclass as? Rating
            }
            catch
            {
                return nil
            }
        }
    func addEvent(idEvent:Int,nameEvent:String,descriptionEvent:String) -> Events? {
        
        
        
        let appdel = UIApplication.shared.delegate as! AppDelegate
        let context = appdel.persistentContainer.viewContext
        let newclass = NSEntityDescription.insertNewObject(forEntityName: "Events", into: context)
        newclass.setValue(idEvent, forKey: "id")
        newclass.setValue(nameEvent, forKey: "name")
        newclass.setValue(descriptionEvent, forKey: "descriptionevent")
        do
        {
            try context.save()
            return newclass as? Events
        }
        catch
        {
            return nil
        }
    }
    func AddComment(commentdesc:String,user:User) -> Comment? {
        
        
        
        let appdel = UIApplication.shared.delegate as! AppDelegate
        let context = appdel.persistentContainer.viewContext
        let newclass = NSEntityDescription.insertNewObject(forEntityName: "Comment", into: context)
        newclass.setValue(commentdesc, forKey: "commentDesc")
        newclass.setValue(pkid(), forKey: "id")
         newclass.setValue(user, forKey: "creater")
        do
        {
            try context.save()
            return newclass as? Comment
        }
        catch
        {
            return nil
        }
    }
    func addUser(phone:String,isuser:Bool, completion: (_ result: Bool,_ error:String)->())  {
    
                if let user:User = checkUserExist(isUser: isuser, phone: phone)
        {
            if (user.isUser == isuser )
            {
                completion(true,"")
            }
            else
            {
                switch(isuser)
                {
                    
                case true:
                    completion(false,"This already registered with different Artist")
                case false:
                    completion(false,"This already registered with different User")
                }
                
                
                
            }
        }
        else
        {
            
            let appdel = UIApplication.shared.delegate as! AppDelegate
            let context = appdel.persistentContainer.viewContext
            let newclass = NSEntityDescription.insertNewObject(forEntityName: "User", into: context)
            newclass.setValue(isuser, forKey: "isUser")
            newclass.setValue(pkid(), forKey: "id")
            newclass.setValue(phone, forKey: "number")

            do
            {
                try context.save()
                //return true
                completion(true,"")
            }
            catch
            {
                completion(false,"Error")
                
                //return false
            }
        }
       
        
    }
    func checkUserExist(isUser:Bool,phone:String) -> User? {
        let appdel = UIApplication.shared.delegate as! AppDelegate
        let context = appdel.persistentContainer.viewContext
        let fetreq = NSFetchRequest<NSFetchRequestResult>(entityName:"User")
        fetreq.returnsObjectsAsFaults=false
        fetreq.predicate = NSPredicate(format: "number = %@",phone)
        do
        {
            let counfod = try context.fetch(fetreq)
            if counfod.count>0 {
                do
                {
                    let objstu:User=counfod[0] as! User

                    return objstu
                }

            }
            
        }
        catch
        {
            return nil
        }
    return nil
    }
    
    //MARK:- update subject to student
    func updateArtist(artistId:Int16,rating:Rating,comment:Comment)->Bool {
        let appdel = UIApplication.shared.delegate as! AppDelegate
        let context = appdel.persistentContainer.viewContext
        let fetreq = NSFetchRequest<NSFetchRequestResult>(entityName:"Artist")
        fetreq.returnsObjectsAsFaults=false
        fetreq.predicate = NSPredicate(format: "id = %d",artistId)

        do
        {
            let counfod = try context.fetch(fetreq)
            if counfod.count>0 {
                let objstu:Artist=counfod[0] as! Artist
                
                let comments = objstu.mutableSetValue(forKey: "comments")
                comments.add(comment)
                objstu.comments = comments
                
                
                let rattings = objstu.mutableSetValue(forKey: "ratings")
                rattings.add(rating)
                objstu.ratings = rattings
                do
                {
                    try context.save()
                    return true
                }
                catch
                {
                    //return false
                }
                
            }
            
        }
        catch
        {
            return true
            
        }
        
        return false
    }

    
    func pkid() -> Int16 {
        let randomNum:UInt16 = UInt16(arc4random_uniform(10000)) // range is 0 to 99
        
        let someInt16:Int = Int(Int16(randomNum))
        return Int16(someInt16)
    }

    }

