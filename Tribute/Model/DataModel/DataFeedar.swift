//
//  DataFeedar.swift
//  Tribute
//
//  Created by saurabh-pc on 09/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit

class DataFeedar: NSObject {
    override init() {
        super.init()
        self.loadd()
    }
    func loadd() {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let documentDirectory = paths[0] as! String
        let path = documentDirectory.appending("Artist.plist")
        let fileManager = FileManager.default
        if(!fileManager.fileExists(atPath: path)){
            if let bundlePath = Bundle.main.path(forResource: "Artist", ofType: "plist"){
                let result:NSArray = NSMutableArray(contentsOfFile: bundlePath)!
                DataBaseManager.sharedInstance.loadArtist(lists: result as! Array<Any>)
                print("Bundle file myData.plist is -> \(result.description)")
                do{
                    try fileManager.copyItem(atPath: bundlePath, toPath: path)
                }catch{
                    print("copy failure.")
                }
            }else{
                print("file myData.plist not found.")
            }
        }else{
            print("file myData.plist already exits at path.")
        }
        
    }
    
}
