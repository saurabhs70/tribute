//
//  Comment+CoreDataProperties.swift
//  Tribute
//
//  Created by saurabh-pc on 10/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//
//

import Foundation
import CoreData


extension Comment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Comment> {
        return NSFetchRequest<Comment>(entityName: "Comment")
    }

    @NSManaged public var commentDesc: String?
    @NSManaged public var id: Int16
    @NSManaged public var creater: User?

}
