//
//  AuthUser.swift
//  Tribute
//
//  Created by saurabh-pc on 09/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit

class AuthUser: NSObject,NSCoding {

    var authuserName:String!
    var authUserPhone:String!
    var authIsUser:Bool!
     init(_authuserName:String,_authUserPhone:String,_authIsUser:Bool) {
        authuserName = _authuserName
        authUserPhone = _authUserPhone
        authIsUser = _authIsUser
        
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let isauthuserName = aDecoder.decodeObject(forKey: "authuserName") as! String
        let isauthUserPhone = aDecoder.decodeObject(forKey: "authUserPhone") as! String
        let isauthIsUser = aDecoder.decodeObject(forKey: "authIsUser") as! Bool
        self.init(_authuserName: isauthuserName, _authUserPhone: isauthUserPhone, _authIsUser: isauthIsUser)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(authuserName, forKey: "authuserName")
        aCoder.encode(authUserPhone, forKey: "authUserPhone")
        aCoder.encode(authIsUser, forKey: "authIsUser")
    }

}
