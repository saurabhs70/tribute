//
//  SideMenuViewController.swift
//  Tribute
//
//  Created by saurabh-pc on 09/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
  var lists = [String]()//["Home","About","Book Events"]
    @IBOutlet weak var tblLeftMenuList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        lists = ["Home","About"]

        let phone =    UserDefaults.standard.object(forKey: "loggedinPhone") as? String
        if let phone1 = phone
        {
            
            let user = DataBaseManager.sharedInstance.checkUserExist(isUser: true, phone: phone1)
            if (!(user?.isUser)!)
            {
                lists = ["Home","About"]
                
            }
            else
            {
                lists = ["Home","About","Book Events"]
            }

        }
        else{
            lists = ["Home","About"]
        }
        tblLeftMenuList.reloadData()
                // Do any additional setup after loading the view.
    }

    @IBAction func btnLogoutClicked(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "loggedinPhone")
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.appCoordinator.start()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SideMenuViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
        cell.lableTitle.text = lists[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row == 2)
        {
            self.hideLeftView(true)

            let appdelegate = UIApplication.shared.delegate as! AppDelegate
          let navigation =   appdelegate.appCoordinator.coordinators["dashboard"] as! DashboardCoordinator
           let dashboard = navigation.mainnavigationController.rootViewController as! UINavigationController
            let dash  = dashboard.viewControllers[0] as! DashBoardViewController
            let set = prepareEvent()
            dash.viewModel.gotoEventListWithData(set: set)
            
          

        }
    }
}
extension SideMenuViewController:EventsCoordinatorDelegate
{
    func dismissEventist() {
        
    }
    
    
}
extension SideMenuViewController
{
    func prepareEvent() -> NSSet {
        var array = [Any]()
        let events = DataBaseManager.sharedInstance.getall(stringforentity: "Artist") as! [Artist]
        for events1 in events
        {
            let events12 = events1 as! Artist
            for ev in events12.events!
            {
                let event = ev as! Events
                array.append(event)
                
            }
            print(events12.events)
        }
        let set = NSSet.init(array: array)
        return set
    }
}
