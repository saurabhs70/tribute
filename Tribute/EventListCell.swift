//
//  EventListCell.swift
//  Tribute
//
//  Created by saurabh-pc on 10/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
protocol EventListCellDelegate:class  {
    func updatetoeventbook(tag:Int)
}

class EventListCell: UITableViewCell {
    weak var delegate:EventListCellDelegate?
    @IBOutlet weak var btnbookEventTapped: UIButton!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func preparecell(index:Int)
    {
        btnbookEventTapped.tag = index
        let phone =    UserDefaults.standard.object(forKey: "loggedinPhone") as? String
        let user = DataBaseManager.sharedInstance.checkUserExist(isUser: true, phone: phone!)
        if (!(user?.isUser)!)
        {
            btnbookEventTapped.isHidden = true
        }
        else{
            btnbookEventTapped.isHidden = false

        }
        btnbookEventTapped.addTarget(self, action: #selector(buttonClicked(_:)), for: UIControlEvents.touchUpInside)
    }
   @objc func buttonClicked(_ sender:UIButton)  {
        delegate?.updatetoeventbook(tag: sender.tag)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
