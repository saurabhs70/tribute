//
//  RateUsViewModel.swift
//  Tribute
//
//  Created by saurabh-pc on 10/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit

class RateUsViewModel: RateUsViewModelProtocol {
    
    var artist:Artist!
    
  weak  var delegateCoordinate: RateUsViewModelCoordinateDelegate?
    init(_artist:Artist) {
        artist = _artist
    }
  weak  var delegateView: RateUsViewModelViewDelegate?
    func submitRate(rating: Double, review: String)
    {
        if (review.count > 0)
        {
          // artist = nil
        }
        // add rating && add review
        
       // update this
        if review.count >= 1, rating > 0
        {
            addrating(rating: rating, review: review)
            artist = nil
            delegateCoordinate?.dismissRate()
            
            //return
        }
        else{
        delegateView?.didErrorforfillFeedback()
        }
        
    }
    func backtapped ()  {
        delegateCoordinate?.dismissRate()
    }
    func addrating(rating: Double, review: String)  {
        let phone =    UserDefaults.standard.object(forKey: "loggedinPhone") as? String
        let user = DataBaseManager.sharedInstance.checkUserExist(isUser: true, phone: phone!)
        let rating = DataBaseManager.sharedInstance.addRating(rating: rating, user: user!)
        let comment = DataBaseManager.sharedInstance.AddComment(commentdesc: review, user: user!)
       _ = DataBaseManager.sharedInstance.updateArtist(artistId: artist.id, rating: rating!, comment: comment!)
    }

}
