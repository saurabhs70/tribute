//
//  RateUsViewModelProtocol.swift
//  Tribute
//
//  Created by saurabh-pc on 10/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
protocol RateUsViewModelCoordinateDelegate:class {
    func dismissRate()
}
protocol RateUsViewModelViewDelegate:class {
    func didErrorforfillFeedback() 
}
protocol RateUsViewModelProtocol {
    var delegateCoordinate:RateUsViewModelCoordinateDelegate?{get set}
    var delegateView:RateUsViewModelViewDelegate?{get set}
    func submitRate(rating:Double,review:String)
      func  backtapped()
}
