//
//  EventViewModelProtocol.swift
//  Tribute
//
//  Created by saurabh-pc on 10/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
protocol EventViewModelCoordinatorDelegate:class {
    func dismissEventVc()
    func gotoBookCoordinate(event:Events) 
}
protocol EventViewModelViewDelegate:class
{
    
}
protocol EventViewModelProtocol {
    var delegateCoordinator:EventViewModelCoordinatorDelegate?{get set}
    var delegateView:EventViewModelViewDelegate?{get set}
      func numberofRows()->Int
    func preopareCell(index:Int) -> Events
    func popView()
    func goToBookEvent(index:Int)
    
}
