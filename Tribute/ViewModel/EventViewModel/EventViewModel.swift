//
//  EventViewModel.swift
//  Tribute
//
//  Created by saurabh-pc on 10/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit

class EventViewModel:EventViewModelProtocol {
    var delegateCoordinator: EventViewModelCoordinatorDelegate?
    
    var delegateView: EventViewModelViewDelegate?
    init(_events:NSSet) {
        events = _events
    }
   private var events:NSSet!

    func numberofRows()->Int  {
        return events.count
    }
    func popView()  {
        delegateCoordinator?.dismissEventVc()
    }
    func preopareCell(index:Int) -> Events {
        //var arr = events.allObjects //Swift Array
        var nsarr = events.allObjects as NSArray
         var obj = nsarr[index] as! Events
        return obj
    }
    func goToBookEvent(index: Int)
   {
    let event = preopareCell(index: index)
    delegateCoordinator?.gotoBookCoordinate(event: event)
    
    }


}
