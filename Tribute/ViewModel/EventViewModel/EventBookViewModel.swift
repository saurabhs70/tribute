//
//  EventBookViewModel.swift
//  Tribute
//
//  Created by saurabh-pc on 10/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit

class EventBookViewModel:EventBookViewModelProtocol {
   weak  var delegateCoordinate: EventBookViewModelCoordinateDelegate?
   weak var delegateView: EventBookViewModelViewDelegate?
    var evnt:Events!
    init(_evnt:Events) {
        evnt = _evnt
    }
    func dissmisseventBook()  {
        delegateCoordinate?.dissmisseventBook()
    }
    func dissmisseventBookandgotolist()  {
        delegateCoordinate?.dissmisseventBookandgotolist()
    }
    var name : String {
        get {
            return evnt.name!
        }
    }
    var descriptionevent : String {
        get {
            return evnt.descriptionevent!
        }
    }

}
