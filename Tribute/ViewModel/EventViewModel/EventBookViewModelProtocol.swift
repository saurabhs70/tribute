//
//  EventBookViewModelProtocol.swift
//  Tribute
//
//  Created by saurabh-pc on 10/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
protocol EventBookViewModelCoordinateDelegate:class {
     func dissmisseventBook()
   func  dissmisseventBookandgotolist()
}
protocol EventBookViewModelViewDelegate:class {
    
}
protocol EventBookViewModelProtocol{
    weak var delegateCoordinate:EventBookViewModelCoordinateDelegate?{get set}
    weak var delegateView:EventBookViewModelViewDelegate?{get set}
    func dissmisseventBook()
    func dissmisseventBookandgotolist()
    var name :String {get}
    var descriptionevent :String {get}
}
