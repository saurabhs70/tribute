//
//  InitialViewModelProtocol.swift
//  Tribute
//
//  Created by saurabh-pc on 09/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
protocol InitialViewModelCoordinaleDelegate:class {
    func pushtoLoginPage(isUser:Bool) 
}
protocol InitialViewModelViewDelegate:class {
    
}
protocol InitialViewModelProtocol {
    var delgateCoordinate:InitialViewModelCoordinaleDelegate?{get set}
    var delgateView:InitialViewModelViewDelegate?{get set}
    func pushtoLoginPage(isUser:Bool) 
}
