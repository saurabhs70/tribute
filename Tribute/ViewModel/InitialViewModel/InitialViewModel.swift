//
//  InitialViewModel.swift
//  Tribute
//
//  Created by saurabh-pc on 09/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit

class InitialViewModel:InitialViewModelProtocol {
    
   weak var delgateCoordinate: InitialViewModelCoordinaleDelegate?
   weak var delgateView: InitialViewModelViewDelegate?
    func pushtoLoginPage(isUser: Bool) {
        delgateCoordinate?.pushtoLoginPage(isUser: isUser)
    }


}
