//
//  DashboardViewModel.swift
//  Tribute
//
//  Created by saurabh-pc on 09/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit

class DashboardViewModel:DashboardViewModelProtocol {
    
    
  weak  var delegateCoordinate: DashboardViewModelCoordinateDelegate?
    
   weak var delegateView: DashboardViewModelViewDelegate?
    
    private var lists = [Artist]()
    private var artist:Artist!
   func prepare() {
        lists = DataBaseManager.sharedInstance.getall(stringforentity: "Artist") as! [Artist]
    }
    var numberofRows:Int
    {
        prepare()
        return lists.count
        
        
    }

    func prepareCell(index:Int) -> Artist {
        return lists[index]
    }
    
    func gotoRating(index:Int)  {
        artist = lists[index]
        if isValidateGotoRating() {
            delegateCoordinate?.gotoRating(artist: lists[index])
        }
        
    }
    func gotoEventList(index: Int) {
        let artist = lists[index] as! Artist
        print(artist.events?.count)
        let event = artist.events as! NSSet // (lists[index] as! Artist).value(forKey: "events")
        delegateCoordinate?.gotoEvent(event: event as! NSSet)
    }

    func isValidateGotoRating()->Bool  {
        let phone =    UserDefaults.standard.object(forKey: "loggedinPhone") as? String
        let user = DataBaseManager.sharedInstance.checkUserExist(isUser: true, phone: phone!)
        if (!(user?.isUser)!)
        {
            delegateView?.didErrorAlreadyRated(errormessage: "Artist Can't rate!")
            return false
        }
        let comments = artist.comments
        for  userinfo in comments! {
            let userto = ((userinfo as! Comment))
            //.value(forKey: "creater") ?? "") as! User
            if(!(userto.creater?.isUser)!)
            {
                delegateView?.didErrorAlreadyRated(errormessage: "Artist Can't rate!")
                return false

            }
            if(user?.id == userto.id)
            {
                delegateView?.didErrorAlreadyRated(errormessage: "You have Already Rated!")
                return false
            }
        }
        return true
    }
    func gotoEventListWithData(set: NSSet) {
        delegateCoordinate?.gotoEvent(event: set)
    }
    
}
