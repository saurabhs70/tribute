//
//  DashboardViewModelProtocol.swift
//  Tribute
//
//  Created by saurabh-pc on 09/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
protocol DashboardViewModelCoordinateDelegate:class {
    func gotoRating(artist:Artist)
    func gotoEvent(event:NSSet)
    //func gotoeventlistwithData()

}
protocol DashboardViewModelViewDelegate:class {
    func didErrorAlreadyRated(errormessage:String)
}
protocol DashboardViewModelProtocol {
    var delegateCoordinate:DashboardViewModelCoordinateDelegate?{get set}
    var delegateView:DashboardViewModelViewDelegate?{get set}
     func prepare()
    var numberofRows:Int{get}
    func prepareCell(index:Int) -> Artist
    func gotoRating(index:Int)
    func gotoEventList(index: Int)
    func gotoEventListWithData(set:NSSet) 

    

}
