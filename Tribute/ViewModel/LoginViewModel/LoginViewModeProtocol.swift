//
//  LoginViewModeProtocol.swift
//  Tribute
//
//  Created by saurabh-pc on 09/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
protocol LoginViewModelCoordinateDelegate:class {
    func present()
    func dissmissLoginvc() 
}
protocol LoginViewModelViewDelegate:class {
    func didUpdateError(iserror:Bool)
    func didPresentError(iserror:String)
}
protocol LoginViewModeProtocol {
    var delegateCoordinate:LoginViewModelCoordinateDelegate?{get set}
    var delegateView:LoginViewModelViewDelegate?{get set}
    func presentWithLoginMode(isUser:Bool)
    func dissmissogin()
    var txt:String {get set}
    func validatelogin(phone:String)
    func prepareAuthUser(phone:String,isUser:Bool,name:String) 
}
