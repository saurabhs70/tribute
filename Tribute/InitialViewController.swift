//
//  ViewController.swift
//  Tribute
//
//  Created by saurabh-pc on 09/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
import LGSideMenuController
class InitialViewController: UIViewController {
    var viewModel:InitialViewModel!
    @IBAction func btnaArtistLoginTapped(_ sender: Any) {
        viewModel.pushtoLoginPage(isUser: false)
    }
    @IBAction func btnUserLoginTapped(_ sender: Any) {
        viewModel.pushtoLoginPage(isUser: true)

    }
    @IBAction func sideMenuTapped(_ sender: Any) {
        showLeftViewAnimated(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delgateView = self
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension InitialViewController:InitialViewModelViewDelegate
{
    
}
