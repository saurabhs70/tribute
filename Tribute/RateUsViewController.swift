//
//  RateUsViewController.swift
//  Tribute
//
//  Created by saurabh-pc on 10/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
import Cosmos
class RateUsViewController: UIViewController {
    var viewModel:RateUsViewModel!
    var ratingvalue:Double = 0
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var txtreview: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
      viewModel.delegateView = self
        rateView.settings.updateOnTouch = true
        rateView.settings.starSize = 30
        rateView.settings.fillMode = .half
        // Set the distance between stars
        rateView.settings.starMargin = 5
        rateView.backgroundColor = UIColor.clear

        rateView.didTouchCosmos = {[weak self] rating in
            print(rating)
            self?.ratingvalue = rating
        }
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        viewModel.backtapped()
    }
    
    @IBAction func rateUsButtonTapped(_ sender: Any) {
        viewModel.submitRate(rating: ratingvalue, review: txtreview.text)
//        rateView.didFinishTouchingCosmos = { rating in
//
//
//            print(rating)
//        }
        

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension RateUsViewController:RateUsViewModelViewDelegate
{
    func didErrorforfillFeedback() {
        let alert = UIAlertController(title: "Error!", message: "Please Rate & comment!", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}
