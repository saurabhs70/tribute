//
//  EventsViewController.swift
//  Tribute
//
//  Created by saurabh-pc on 10/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit

class EventsViewController: UIViewController {
    var viewModel:EventViewModel!
    @IBOutlet weak var tbllist: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
       viewModel.delegateView = self
        self.tbllist.rowHeight = UITableViewAutomaticDimension;
        self.tbllist.estimatedRowHeight = 100;
        // Do any additional setup after loading the view.
    }

    @IBAction func btnGotobackTapped(_ sender: Any) {
        viewModel.popView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    
}
extension EventsViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberofRows()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "EventListCell", for: indexPath) as! EventListCell
        let event = viewModel.preopareCell(index: indexPath.row)
        cell.eventTitle.text = event.name
        cell.eventDescription.text = event.descriptionevent
        cell.delegate = self
        cell.selectionStyle = .none
        cell.preparecell(index: indexPath.row)
        return cell
    }
    
}
extension EventsViewController:EventViewModelViewDelegate
{
    
}
extension EventsViewController:EventListCellDelegate
{
    func updatetoeventbook(tag: Int) {
        viewModel.goToBookEvent(index: tag)
    }
   
    
    
}
