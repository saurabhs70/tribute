//
//  LoginViewController.swift
//  Tribute
//
//  Created by saurabh-pc on 09/06/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    var viewModel:LoginViewModel!
    @IBAction func dismissButtonTapped(_ sender: Any) {
        viewModel.dissmissogin()
    }
    @IBOutlet weak var loginErrorView: UIView!
    @IBOutlet weak var loginTextField: UITextField!
    @IBAction func loginButtonTapped(_ sender: Any) {
        viewModel.validatelogin(phone: loginTextField.text!)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegateView = self
        loginTextField.addTarget(self, action: #selector(updatetext(_:)), for: UIControlEvents.editingChanged)
        // Do any additional setup after loading the view.
    }
    @objc func updatetext(_ txt:UITextField)
    {
        viewModel.txt =  txt.text!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LoginViewController:LoginViewModelViewDelegate
{
    func didPresentError(iserror: String) {
        let alert = UIAlertController(title: "Error!", message: iserror, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        self.present(alert, animated: true, completion: nil)

    }
    
    func didUpdateError(iserror: Bool) {
        if iserror == true
        {
            loginErrorView.backgroundColor = UIColor.red
        }
        else
        {
            loginErrorView.backgroundColor = UIColor.green

        }
    }
    
    
}
